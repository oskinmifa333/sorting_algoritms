# Тестирование функций сортировок

import pytest
from main import *


@pytest.mark.parametrize("lst, excepted_result", [(res_arr, res_1_arr), (res_arr_100, res_1_arr_100),
                                                  (res_arr_1000, res_1_arr_1000)])
def test_bubble_sort(lst, excepted_result):
    if type(lst) not in [list]:
        raise TypeError('Типовая ошибка. lst не является списком')
    if type(excepted_result) not in [list]:
        raise TypeError('Типовая ошибка. excepted_result не является списком')
    assert excepted_result == sorted(lst)


@pytest.mark.parametrize("lst, excepted_result", [(res_arr, res_2_arr), (res_arr_100, res_2_arr_100),
                                                  (res_arr_1000, res_2_arr_1000)])
def test_insertion_sort(lst, excepted_result):
    if type(lst) not in [list]:
        raise TypeError('Типовая ошибка. lst не является списком')
    if type(excepted_result) not in [list]:
        raise TypeError('Типовая ошибка. excepted_result не является списком')
    assert excepted_result == sorted(lst)


@pytest.mark.parametrize("lst, excepted_result", [(res_arr, res_2_arr), (res_arr_100, res_3_arr_100),
                                                  (res_arr_1000, res_3_arr_1000)])
def test_shell_sort(lst, excepted_result):
    if type(lst) not in [list]:
        raise TypeError('Типовая ошибка. lst не является списком')
    if type(excepted_result) not in [list]:
        raise TypeError('Типовая ошибка. excepted_result не является списком')
    assert excepted_result == sorted(lst)


@pytest.mark.parametrize("lst, excepted_result", [(res_arr, res_2_arr), (res_arr_100, res_4_arr_100),
                                                  (res_arr_1000, res_4_arr_1000)])
def test_quick_sort(lst, excepted_result):
    if type(lst) not in [list]:
        raise TypeError('Типовая ошибка. lst не является списком')
    if type(excepted_result) not in [list]:
        raise TypeError('Типовая ошибка. excepted_result не является списком')
    assert excepted_result == sorted(lst)


@pytest.mark.parametrize("lst, back_res", [(res_arr, back_arr)])
def test_back_arr(back_res, lst):
    if type(lst) not in [list]:
        raise TypeError('Типовая ошибка. lst не является списком')
    if type(back_res) not in [list]:
        raise TypeError('Типовая ошибка. back_arr не является списком')
    assert back_res == sorted(lst, reverse=True)
